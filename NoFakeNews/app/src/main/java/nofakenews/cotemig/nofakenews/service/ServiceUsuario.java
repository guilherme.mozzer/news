package nofakenews.cotemig.nofakenews.service;

import nofakenews.cotemig.nofakenews.models.RespostaUsuario;
import nofakenews.cotemig.nofakenews.models.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServiceUsuario {

    @POST("UsuarioController/cadastrarUsuario")
    Call<RespostaUsuario> insertCadastro(@Body Usuario u);

    @POST("UsuarioController/loginUsuario")
    Call<RespostaUsuario> loginUsuario(@Body Usuario u);

}
