package nofakenews.cotemig.nofakenews.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nofakenews.cotemig.nofakenews.R;
import nofakenews.cotemig.nofakenews.utils.Funcao;

public class esqueceusenhaActivity extends AppCompatActivity {

    @BindView(R.id.email3)
    EditText email;

    String mensagem;
    Boolean status;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_esqueceusenha);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.buttonesqueceusenha)
    public void TelaAlterarsenha() {
        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        if(email.getText().equals("") || !Funcao.IsEmail(email.getText().toString())){
            dlgAlert.setMessage("Preencha o email corretamente! ");
            dlgAlert.setTitle("Email inválido !");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        }
        else
        {

            dlgAlert.setMessage("Um email para a troca de senha foi enviado no endereço requisitado ! ");
            dlgAlert.setTitle("Solitação de troca de senha enviada com sucesso !");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
            dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 3000);


        }
    }

}
