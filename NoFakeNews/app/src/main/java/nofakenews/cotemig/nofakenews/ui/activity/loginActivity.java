package nofakenews.cotemig.nofakenews.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nofakenews.cotemig.nofakenews.R;
import nofakenews.cotemig.nofakenews.app.ApplicationNews;
import nofakenews.cotemig.nofakenews.models.RespostaUsuario;
import nofakenews.cotemig.nofakenews.models.Usuario;
import nofakenews.cotemig.nofakenews.service.ServiceUsuario;
import nofakenews.cotemig.nofakenews.utils.Funcao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by 71700196 on 24/10/2018.
 */

public class loginActivity extends AppCompatActivity {
    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    EditText password;

    Boolean status;

    String mensagem;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.bntlogar)
    public void loginclick() {

        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

        if (email.getText().toString().equals("")) {
            dlgAlert.setMessage("Digite seu E-mail! ");
            dlgAlert.setTitle("E-mail inválido:");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else if (!Funcao.IsEmail(email.getText().toString())) {

            dlgAlert.setMessage("Digite um E-mail válido! ");
            dlgAlert.setTitle("E-mail inválido:");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else if (password.getText().toString().equals("")) {
            dlgAlert.setMessage("Digite sua senha! ");
            dlgAlert.setTitle("Senha inválido:");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
        } else {

            Intent i = new Intent(loginActivity.this, newsActivity.class);
            startActivity(i);


            Usuario u = new Usuario();


            u.setEmail(email.getText().toString());
            u.setSenha(password.getText().toString());

            ServiceUsuario su = ApplicationNews.getInstance().getServiceUsuario();

            Call<RespostaUsuario> call = su.loginUsuario(u);


            call.enqueue(new Callback<RespostaUsuario>() {
                @Override
                public void onResponse(Call<RespostaUsuario> call, Response<RespostaUsuario> response) {

                    status = response.body().getStatus();
                    mensagem = response.body().getMensagem();

                    if (status) {
                        Intent i = new Intent(loginActivity.this, newsActivity.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(loginActivity.this, mensagem, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RespostaUsuario> call, Throwable t) {
                    Toast.makeText(loginActivity.this, "Error ao Logar!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @OnClick(R.id.bntcadastrar)
    public void TelaCadastro(){
        Intent i = new Intent(this, cadastroActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.bntesqueuceusenha)
    public void TelaEsquesisenha(){
        Intent i = new Intent(this, esqueceusenhaActivity.class);
        startActivity(i);
    }
}