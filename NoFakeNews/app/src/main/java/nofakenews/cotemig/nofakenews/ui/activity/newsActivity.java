package nofakenews.cotemig.nofakenews.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nofakenews.cotemig.nofakenews.R;
import nofakenews.cotemig.nofakenews.app.ApplicationNews;
import nofakenews.cotemig.nofakenews.models.Artigo;
import nofakenews.cotemig.nofakenews.models.Recurso;
import nofakenews.cotemig.nofakenews.models.RespostaNews;
import nofakenews.cotemig.nofakenews.service.ServiceNews;
import nofakenews.cotemig.nofakenews.ui.adapters.noticiaAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class newsActivity extends AppCompatActivity {

    @BindView(R.id.listanews)
    ListView listView;

    @BindView(R.id.buscarnews)
    EditText bucarchave;

    List<Artigo> artigoList;

    public String regiao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);

        CarregarLista();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Recurso recurso = (Recurso) parent.getAdapter().getItem(position);
                Artigo artigo = (Artigo) parent.getAdapter().getItem(position);


                String site = recurso.getNome();
                String titulo = artigo.getTitulo();
                String autor = artigo.getAutor();
                String descricao = artigo.getDescricao();
                String url = artigo.getURL();
                String imagem = artigo.getImagem();
                String data = artigo.getDataPublicacao();
                String conteudo = artigo.getConteudo();

                Intent i = new Intent(getApplicationContext(), newscompletaActivity.class);
                i.putExtra("sitenoticia", site);
                i.putExtra("titulo", titulo);
                i.putExtra("autor", autor);
                i.putExtra("descricao", descricao);
                i.putExtra("url", url);
                i.putExtra("imagem", imagem);
                i.putExtra("data", data);
                i.putExtra("conteudo", conteudo);
                startActivity(i);

            }
        });
    }

    public void CarregarLista(){
        ServiceNews sn = ApplicationNews.getInstance().getServiceNews();

        Call<RespostaNews> call = sn.obterArtigos("br", ApplicationNews.APP_ID);
        call.enqueue(new Callback<RespostaNews>() {
            @Override
            public void onResponse(Call<RespostaNews> call, Response<RespostaNews> response) {
                artigoList = response.body().getArtigos();
                noticiaAdapter adapter = new noticiaAdapter(newsActivity.this, artigoList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<RespostaNews> call, Throwable t) {
                Toast.makeText(newsActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }



    @OnClick(R.id.bntbuscarnews)
    public void Buscar() {

        String palavra = bucarchave.getText().toString();

        if (!bucarchave.getText().toString().equals("")) {

            ServiceNews sn = ApplicationNews.getInstance().getServiceNews();

            Call<RespostaNews> call = sn.obterArtigosbusca(palavra, ApplicationNews.APP_ID);

            call.enqueue(new Callback<RespostaNews>() {
                @Override
                public void onResponse(Call<RespostaNews> call, Response<RespostaNews> response) {
                    artigoList = response.body().getArtigos();
                    noticiaAdapter adapter = new noticiaAdapter(newsActivity.this, artigoList);
                    listView.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<RespostaNews> call, Throwable t) {

                }
            });
        }
    }


}

