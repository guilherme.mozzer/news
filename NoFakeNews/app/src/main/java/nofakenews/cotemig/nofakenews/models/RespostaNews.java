package nofakenews.cotemig.nofakenews.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RespostaNews implements Serializable {
    @SerializedName("status")
    private Boolean Status;
    @SerializedName("totalResults")
    private int TotalResultados;
    @SerializedName("articles")
    private List<Artigo> Artigos;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public int getTotalResultados() {
        return TotalResultados;
    }

    public void setTotalResultados(int totalResultados) {
        TotalResultados = totalResultados;
    }

    public List<Artigo> getArtigos() {
        return Artigos;
    }

    public void setArtigos(List<Artigo> artigos) {
        Artigos = artigos;
    }
}
