package nofakenews.cotemig.nofakenews.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import nofakenews.cotemig.nofakenews.R;
import nofakenews.cotemig.nofakenews.models.Artigo;
import nofakenews.cotemig.nofakenews.models.RespostaNews;

public class noticiaAdapter extends BaseAdapter {

    private Context context;
    private List<Artigo> lnews;

    public noticiaAdapter(Context context, List<Artigo> lnews) {
        this.context = context;
        this.lnews = lnews;
    }

    @Override
    public int getCount() {
        return lnews.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Artigo news = lnews.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.item_news, null);

        TextView titulo = v.findViewById(R.id.titulonews);
        titulo.setText(news.getTitulo());

        TextView autor = v.findViewById(R.id.autornews);
        autor.setText(news.getAutor());

        TextView data = v.findViewById(R.id.datanews);
        data.setText(news.getDataPublicacao());

        TextView descricao = v.findViewById(R.id.descricaonews);
        descricao.setText(news.getDescricao());

        ImageView imagem = v.findViewById(R.id.imagemnews);
        Glide.with(context).load(news.getImagem()).into(imagem);

        return v;
    }

}
