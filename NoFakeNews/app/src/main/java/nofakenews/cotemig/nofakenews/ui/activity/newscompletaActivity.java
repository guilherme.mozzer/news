package nofakenews.cotemig.nofakenews.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nofakenews.cotemig.nofakenews.R;

public class newscompletaActivity extends AppCompatActivity {

    @BindView(R.id.recurso)
    TextView site;

    @BindView(R.id.titulo)
    TextView titulo;

    @BindView(R.id.autor)
    TextView autor;

    @BindView(R.id.data)
    TextView data;

    @BindView(R.id.descricao)
    TextView decricao;

    @BindView(R.id.conteudo)
    TextView conteudo;

    @BindView(R.id.url)
    TextView url;

    @BindView(R.id.imagem)
    ImageView imagem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newscompleta);

        ButterKnife.bind(this);

        String isite = getIntent().getStringExtra("sitenoticia");
        String ititulo = getIntent().getStringExtra("titulo");
        String idescricao = getIntent().getStringExtra("descricao");
        String iautor = getIntent().getStringExtra("autor");
        String iurl = getIntent().getStringExtra("url");
        String iimagem = getIntent().getStringExtra("imagem");
        String idata = getIntent().getStringExtra("data");
        String iconteudo = getIntent().getStringExtra("conteudo");

        site.setText(isite);
        titulo.setText(ititulo);
        autor.setText(iautor);
        data.setText(idata);
        decricao.setText(idescricao);
        conteudo.setText(iconteudo);
        url.setText(iurl);

        Glide.

        imagem.setText(iimagem);

    }


}
