package nofakenews.cotemig.nofakenews.ui.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import nofakenews.cotemig.nofakenews.R;

public class logoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        //aplica um splash na tela para iniciar
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mostrarMainActivity();
            }
        }, 8000);

    }
    private void mostrarMainActivity() {
        Intent i = new Intent(this, loginActivity.class);
        startActivity(i);
        finish();
    }
}
