package nofakenews.cotemig.nofakenews.service;

import nofakenews.cotemig.nofakenews.models.RespostaNews;
import nofakenews.cotemig.nofakenews.models.RespostaUsuario;
import nofakenews.cotemig.nofakenews.models.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServiceNews {
    @GET("top-headlines")
    Call<RespostaNews> obterArtigos(@Query("country") String country, @Query("apiKey") String apikey);

    @GET("everything")
    Call<RespostaNews> obterArtigosbusca(@Query("q") String q, @Query("apiKey") String apikey);
}