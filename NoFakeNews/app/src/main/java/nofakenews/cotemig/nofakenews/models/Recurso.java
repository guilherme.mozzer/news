package nofakenews.cotemig.nofakenews.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Recurso implements Serializable {
    @SerializedName("id")
    private int ID;
    @SerializedName("nome")
    private String Nome;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }
}
