package nofakenews.cotemig.nofakenews.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import nofakenews.cotemig.nofakenews.R;
import nofakenews.cotemig.nofakenews.app.ApplicationNews;
import nofakenews.cotemig.nofakenews.models.RespostaUsuario;
import nofakenews.cotemig.nofakenews.models.Usuario;
import nofakenews.cotemig.nofakenews.service.ServiceUsuario;
import nofakenews.cotemig.nofakenews.utils.Funcao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class cadastroActivity extends AppCompatActivity {

    @BindView(R.id.nomecadastro)
    EditText nome;

    @BindView(R.id.emailcadastro)
    EditText email;

    @BindView(R.id.senhacadastro)
    EditText senha;

    @BindView(R.id.confirmsenhacadastro)
    EditText confirmsenha;


    Usuario listausu;
    String mensagem;
    Boolean status;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.buttoncadastrar)
    public void finalizaCadastro() {
        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

        if (nome.getText().toString().equals("")) {
            dlgAlert.setMessage("Digite seu Nome! ");
            dlgAlert.setTitle("Nome inválido:");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else if (!Funcao.IsEmail(email.getText().toString())) {

            dlgAlert.setMessage("Digite um E-mail válido! ");
            dlgAlert.setTitle("E-mail inválido:");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else if (!senha.getText().toString().equals(confirmsenha.getText().toString()) || senha.getText().toString().equals("")) {
            String msg = !senha.getText().toString().equals(confirmsenha.getText().toString()) ? "As Senhas não são iguais!" : "Digite a Senha!";

            dlgAlert.setMessage(msg);
            dlgAlert.setTitle("Senha inválido: ");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else {

            Usuario u = new Usuario();

            u.setNome(nome.getText().toString());
            u.setEmail(email.getText().toString());
            u.setSenha(senha.getText().toString());

            ServiceUsuario su = ApplicationNews.getInstance().getServiceUsuario();

            Call<RespostaUsuario> call = su.insertCadastro(u);


            call.enqueue(new Callback<RespostaUsuario>() {
                @Override
                public void onResponse(Call<RespostaUsuario> call, Response<RespostaUsuario> response) {

                    listausu = response.body().getUsuario();
                    status = response.body().getStatus();
                    mensagem = response.body().getMensagem();

                    if (status == true) {

                        Intent i = new Intent(cadastroActivity.this, loginActivity.class);
                        startActivity(i);
                    }
                }

                @Override
                public void onFailure(Call<RespostaUsuario> call, Throwable t) {
                    Toast.makeText(cadastroActivity.this, "Erro ao Cadastrar Usuário!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}