package nofakenews.cotemig.nofakenews.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Artigo implements Serializable {
    @SerializedName("source")
    private Recurso recursoNoticia = new Recurso();

    @SerializedName("author")
    private String Autor;

    @SerializedName("title")
    private String Titulo;

    @SerializedName("description")
    private String Descricao;

    @SerializedName("url")
    private String URL;

    @SerializedName("urlToImage")
    private String Imagem;

    @SerializedName("publishedAt")
    private String DataPublicacao;

    @SerializedName("content")
    private String Conteudo;

    public Recurso getRecursoNoticia() {
        return recursoNoticia;
    }

    public void setRecursoNoticia(Recurso recursoNoticia) {
        this.recursoNoticia = recursoNoticia;
    }

    public String getAutor() {
        return Autor;
    }

    public void setAutor(String autor) {
        Autor = autor;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getImagem() {
        return Imagem;
    }

    public void setImagem(String imagem) {
        Imagem = imagem;
    }

    public String getDataPublicacao() {
        return DataPublicacao;
    }

    public void setDataPublicacao(String dataPublicacao) {
        DataPublicacao = dataPublicacao;
    }

    public String getConteudo() {
        return Conteudo;
    }

    public void setConteudo(String conteudo) {
        Conteudo = conteudo;
    }
}
