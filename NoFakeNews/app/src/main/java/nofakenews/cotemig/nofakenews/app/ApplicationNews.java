package nofakenews.cotemig.nofakenews.app;

import android.app.Application;

import nofakenews.cotemig.nofakenews.service.RestService;
import nofakenews.cotemig.nofakenews.service.ServiceNews;
import nofakenews.cotemig.nofakenews.service.ServiceUsuario;

public class ApplicationNews extends Application {
    private static final String URL_USUARIO = "http://172.16.212.136:6040/api/";
    private static final boolean Noticias = false;
    //private static final String URL_USUARIO = "http://192.168.1.4:6040/api/";
    private static final String URL_NOTICIAS = "https://newsapi.org/v2/";
    public static final String APP_ID = "b0215cb8414f4f56932c218ce6416e07";
    private static final String URL = Noticias ? URL_NOTICIAS : URL_USUARIO;

    private static ApplicationNews instance;

    private ServiceUsuario serviceUsuario;
    private ServiceNews serviceNews;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        createServices();
    }

    private void createServices(){
        serviceUsuario = (new RestService(URL_USUARIO).getService(ServiceUsuario.class));
        serviceNews = (new RestService(URL_NOTICIAS).getService(ServiceNews.class));
    }

    public static ApplicationNews getInstance() {
        return instance;
    }

    public ServiceUsuario getServiceUsuario(){
        return serviceUsuario;
    }
    public ServiceNews getServiceNews(){
        return serviceNews;
    }

}

