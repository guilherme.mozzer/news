package nofakenews.cotemig.nofakenews.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RespostaUsuario implements Serializable{

    private Boolean Status;
    private String Mensagem;
    @SerializedName("Json")
    private Usuario usuario;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getMensagem() {
        return Mensagem;
    }

    public void setMensagem(String mensagem) {
        Mensagem = mensagem;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
