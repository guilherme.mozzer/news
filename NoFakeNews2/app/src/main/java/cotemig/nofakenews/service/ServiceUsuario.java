package cotemig.nofakenews.service;

import cotemig.nofakenews.model.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServiceUsuario {

    @POST("user/registrar")
    Call<Usuario> cadastro(@Body Usuario u );

    @POST("user/login")
    Call<Usuario> login(@Body Usuario u);
}
