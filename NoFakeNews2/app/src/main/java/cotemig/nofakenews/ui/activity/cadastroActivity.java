package cotemig.nofakenews.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cotemig.nofakenews.R;
import cotemig.nofakenews.app.ApplicationNews;
import cotemig.nofakenews.model.Usuario;
import cotemig.nofakenews.service.ServiceUsuario;
import cotemig.nofakenews.util.Funcao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class cadastroActivity extends AppCompatActivity {

    @BindView(R.id.emailcadastro)
    EditText email;

    @BindView(R.id.senhacadastro)
    EditText senha;

    @BindView(R.id.nomecadastro)
    EditText nome;

    @BindView(R.id.nickcadastro)
    EditText nickname;

    @BindView(R.id.datanascimentocadastro)
    EditText datanascimento;

    @BindView(R.id.sexocadastro)
    EditText sexo;

    String sobrenome, name;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.buttoncadastrar)
    public void finalizaCadastro() {
        final AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
        String[] array = nome.getText().toString().split(" ");

        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                name = array[0];
            } else {
                sobrenome = array[i];
            }
        }
        String mensagem = (nome.getText().toString().equals("") ? "Insira seu nome completo !" :
                email.getText().toString().equals("") ? "Insira seu email !" :
                        !Funcao.IsEmail(email.getText().toString()) ? "Email Invalido !" :
                                senha.getText().toString().equals("") ? "Insira uma senha !" :
                                        nickname.getText().toString().equals("") ? "Insira um nickname" :
                                                sexo.getText().toString().equals("") ? "Insira seu sexo !" :
                                                        datanascimento.getText().toString().equals("") ? "Insira sua data de Nascimento" : ""
        );

        if (!mensagem.equals("")) {
            dlgAlert.setMessage(mensagem);
            dlgAlert.setTitle("Erro !");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else {

            Usuario u = new Usuario();

            u.setEmail(email.getText().toString());
            u.setSenha(senha.getText().toString());
            u.setNome(name);
            u.setSobrenome(sobrenome);
            u.setNickname(nickname.getText().toString());
            u.setDt_nascimento("00.00.0000");
            if(sexo.getText().toString().equals("Masculino") || sexo.getText().toString().equals("masculino") || sexo.getText().toString().equals("MASCULINO"))
            {
                u.setSexo("masc");
            }
            else
            {
                u.setSexo("outros");
            }
            u.setTipo_usuario(1);

            ServiceUsuario s = ApplicationNews.getInstance().getServiceUsuario();
            Call<Usuario> call = s.cadastro(u);
            call.enqueue(new Callback<Usuario>() {
                @Override
                public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                    if(response.code() == 200){


                        Toast.makeText(cadastroActivity.this, "Cadastrado com sucesso !", Toast.LENGTH_LONG).show();

                    }else if(response.code() == 400){

                        Toast.makeText(cadastroActivity.this, response.message(), Toast.LENGTH_LONG).show();


                    } else{
                        Toast.makeText(cadastroActivity.this, "error", Toast.LENGTH_LONG).show();
                    }

                }
                @Override
                public void onFailure(Call<Usuario> call, Throwable t) {
                    Toast.makeText(cadastroActivity.this, "Ocorreu algum erro ", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

}
